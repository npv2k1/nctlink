import requests
import re
nct_uuid='D3041C08D4984153AA1C408A31583DAA'
NTC_AUTH_JWT='eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODkyMDYxMDgsImxvZ2luTWV0aG9kIjoiMiIsInVzZXJJZCI6IjM3MDk4MTI2IiwibmJmIjoxNTg2NjE0MTA4LCJpYXQiOjE1ODY2MTQxMDgsImRldmljZUlkIjoiRDMwNDFDMDhENDk4NDE1M0FBMUM0MDhBMzE1ODNEQUEifQ.lbUo1m-Kns9-Z5_Kn8XdFXtL1NH3Znq2wAobTgruM0k'
cookiesnct='nct_uuid={}; NCT_AUTH_JWT={}'.format(nct_uuid, NTC_AUTH_JWT)

def getlinkflac(url):
    res=[]
    nctlink=url
    a=nctlink.split('.')
    idmp3=a[len(a)-2]
    cookies=cookiesnct
    Header = {
    'Host':'www.nhaccuatui.com',
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0',
    'Accept':'application/json, text/javascript, */*; q=0.01',
    'Accept-Language':'vi-VN,vi;q=0.8,en-US;q=0.5,en;q=0.3',
    'Accept-Encoding':'gzip, deflate, br',
    'X-Requested-With':'XMLHttpRequest',
    'DNT':'1',
    'Cookie':cookiesnct,
    'Connection':'keep-alive',
    'Referer':nctlink,
    'TE':'Trailers',
    }
    getlink='https://www.nhaccuatui.com/download/song/'+idmp3+'_lossless'
    a = requests.get(getlink, headers=Header)
    b=re.search(r"\"stream_url\":\"(.*?)\"",a.text)
    regex = r"\\"
    if b:
        test_str = b.group(1)
    subst = ""
    result = re.sub(regex, subst, test_str, 0, re.MULTILINE)
    aa=re.search(r'\/\/.*\/(.*?)\?',result)
    res.append(result)
    res.append(aa.group(1))
    if(res[1]=='waiting_approve'):
        res1=getlinkmp3(url)
        return res1
    else:
        return res
def getlinkmp3(url):
    res=[]
    nctlink=url
    a=nctlink.split('.')
    idmp3=a[len(a)-2]
    cookies=cookiesnct
    Header = {
    'Host':'www.nhaccuatui.com',
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0',
    'Accept':'application/json, text/javascript, */*; q=0.01',
    'Accept-Language':'vi-VN,vi;q=0.8,en-US;q=0.5,en;q=0.3',
    'Accept-Encoding':'gzip, deflate, br',
    'X-Requested-With':'XMLHttpRequest',
    'DNT':'1',
    'Cookie':cookiesnct,
    'Connection':'keep-alive',
    'Referer':nctlink,
    'TE':'Trailers',
    }
    getlink='https://www.nhaccuatui.com/download/song/'+idmp3
    a = requests.get(getlink, headers=Header)
    b=re.search(r"\"stream_url\":\"(.*?)\"",a.text)
    regex = r"\\"
    if b:
        test_str = b.group(1)
    subst = ""
    result = re.sub(regex, subst, test_str, 0, re.MULTILINE)
    aa=re.search(r'\/\/.*\/(.*?)\?',result)
    res.append(result)
    res.append(aa.group(1))
    return res
#get link list
def getlinklist(url):
    '''nhap url playlist '''
    import requests
    import re
    playlist=url
    a=requests.get(playlist)
    regex = r"content=\"(https://www\.nhaccuatui\.com/bai-hat/.*.html)\""
    b=re.findall(regex,a.text)
    return b
def download(url,filename):
    a=requests.get(url)
    with open(filename,'wb') as file:
        for block in a.iter_content(chunk_size = 1024):
            if block:
                file.write(block)


if __name__ == "__main__":
    x=input('url: ')
    info=re.search(r'\.com\/(.*?)\/',x)
    infor=info.group(1)
    #kho
    if infor=='playlist':
        print("Download play list")
        a1=getlinklist(x)
        for i in a1:
            try:
                a2=getlinkflac(i)
                print(a2[1])
                download(a2[0],a2[1])
                print('continue')
            except:
                print(a2[1])
                print('eror')
    #bh
    elif infor=='bai-hat':
        print('Download bai hat')
        try:
            a2=getlinkflac(x)
            print(a2)
            print(a2[1])
            download(a2[0],a2[1])
            print('continue')
        except:
            print(a2)
            print('eror')





